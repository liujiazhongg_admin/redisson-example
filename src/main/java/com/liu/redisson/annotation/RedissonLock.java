package com.liu.redisson.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author liujiazhong
 * @date 2020/4/23 11:23
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface RedissonLock {

    /**
     * 锁定参数表达式
     */
    String lockParamExpression();

    /**
     * 锁自动释放时间(秒)
     */
    int leaseTime() default 60;

}

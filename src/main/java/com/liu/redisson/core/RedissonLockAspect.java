package com.liu.redisson.core;

import com.liu.redisson.annotation.RedissonLock;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.context.expression.MethodBasedEvaluationContext;
import org.springframework.core.DefaultParameterNameDiscoverer;
import org.springframework.core.annotation.Order;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

/**
 * @see RedissonLock
 *
 * @author liujiazhong
 * @date 2020/4/23 11:33
 */
@Slf4j
@Aspect
@Order(1)
@Component
public class RedissonLockAspect {

    private static final Integer LOCK_WAIT_TIME = 5;
    private static final String KEY_PREFIX = "stock:quantity:update:lock:{%s}";

    private final RedissonClient redissonClient;

    public RedissonLockAspect(RedissonClient redissonClient) {
        this.redissonClient = redissonClient;
    }

    @Around("@annotation(redissonLock)")
    public Object around(ProceedingJoinPoint joinPoint, RedissonLock redissonLock) throws Throwable {
        validRedissonLock(redissonLock);

        Long productId = parseExpression(joinPoint, redissonLock);
        String key = String.format(KEY_PREFIX, productId);

        Object obj;
        RLock lock = redissonClient.getLock(key);
        boolean lockResult = lock.tryLock(LOCK_WAIT_TIME, redissonLock.leaseTime(), TimeUnit.SECONDS);
        if (lockResult) {
            log.info("acquire the lock:{}", key);
            try {
                obj = joinPoint.proceed();
            } finally {
                unlock(lock);
            }
            log.info("releases the lock:{}", key);
        } else {
            throw new RuntimeException(String.format("try lock fail:productId:%s", productId));
        }
        return obj;
    }

    private void validRedissonLock(RedissonLock redissonLock) {
        if (StringUtils.isBlank(redissonLock.lockParamExpression())) {
            throw new RuntimeException("no lock param expression.");
        }
    }

    private Method getTargetMethod(ProceedingJoinPoint pjp) throws NoSuchMethodException {
        Signature signature = pjp.getSignature();
        MethodSignature methodSignature = (MethodSignature)signature;
        Method agentMethod = methodSignature.getMethod();
        return pjp.getTarget().getClass().getMethod(agentMethod.getName(),agentMethod.getParameterTypes());
    }

    private Long parseExpression(ProceedingJoinPoint joinPoint, RedissonLock redissonLock) throws NoSuchMethodException {
        String lockParam = redissonLock.lockParamExpression();
        Method targetMethod = getTargetMethod(joinPoint);
        ExpressionParser parser = new SpelExpressionParser();
        EvaluationContext context = new MethodBasedEvaluationContext(new Object(), targetMethod, joinPoint.getArgs(),
                new DefaultParameterNameDiscoverer());
        Expression expression = parser.parseExpression(lockParam);
        return expression.getValue(context, Long.class);
    }

    private void unlock(RLock lock) {
        try {
            lock.unlock();
        } catch (Exception e) {
            log.error("unlock exception.", e);
            throw new RuntimeException("unlock exception.");
        }
    }

}

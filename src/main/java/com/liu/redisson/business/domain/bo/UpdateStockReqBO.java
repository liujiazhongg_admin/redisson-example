package com.liu.redisson.business.domain.bo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author liujiazhong
 * @date 2020/4/23 11:34
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UpdateStockReqBO {

    private Long productId;

}

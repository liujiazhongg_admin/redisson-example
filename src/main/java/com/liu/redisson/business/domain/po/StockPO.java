package com.liu.redisson.business.domain.po;

import lombok.Data;

import javax.persistence.*;

/**
 * @author liujiazhong
 * @date 2020/4/23 11:32
 */
@Data
@Entity
@Table(name = "demo_stock")
public class StockPO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "bigint(20) comment '订单id'")
    private Long id;

    @Column(columnDefinition = "bigint(20) comment '商品id'")
    private Long productId;

    @Column
    private Integer quantity;

}

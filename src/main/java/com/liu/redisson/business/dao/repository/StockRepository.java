package com.liu.redisson.business.dao.repository;

import com.liu.redisson.business.domain.po.StockPO;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author liujiazhong
 * @date 2020/4/23 11:33
 */
public interface StockRepository extends JpaRepository<StockPO, Long> {

    /**
     * 通过商品id查库存
     * @param productId 商品id
     * @return 库存
     */
    StockPO findByProductId(Long productId);

}

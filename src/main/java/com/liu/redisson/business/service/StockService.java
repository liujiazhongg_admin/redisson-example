package com.liu.redisson.business.service;

import com.liu.redisson.annotation.RedissonLock;
import com.liu.redisson.business.dao.repository.StockRepository;
import com.liu.redisson.business.domain.bo.UpdateStockReqBO;
import com.liu.redisson.business.domain.po.StockPO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * @author liujiazhong
 * @date 2020/4/23 11:35
 */
@Slf4j
@Service
public class StockService {

    private final StockRepository stockRepository;

    public StockService(StockRepository stockRepository) {
        this.stockRepository = stockRepository;
    }

    @RedissonLock(lockParamExpression = "#p0.productId")
    @Transactional(rollbackOn = Exception.class)
    public void doStock(UpdateStockReqBO request) {
        StockPO stock = stockRepository.findByProductId(request.getProductId());
        stock.setQuantity(stock.getQuantity() + 1);
        stockRepository.save(stock);
    }

}
